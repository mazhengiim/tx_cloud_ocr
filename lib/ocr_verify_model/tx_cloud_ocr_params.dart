import 'dart:convert';
TxCloudOcrParams txCloudOcrParamsFromJson(String str) =>
    TxCloudOcrParams.fromJson(json.decode(str));

String txCloudOcrParamsToJson(TxCloudOcrParams data) =>
    json.encode(data.toJson());

class TxCloudOcrParams {
  TxCloudOcrParams({
    this.nonce = '',
    this.userId = '',
    this.keyLicence = '',
    this.appId = '',
    this.orderNo = '',
    this.sign = '',
    this.openApiAppVersion = '1.0.0'
  });

  String nonce;
  String userId;
  String keyLicence;
  String appId;
  String orderNo;
  String sign;
  String openApiAppVersion;

  factory TxCloudOcrParams.fromJson(Map<String, dynamic> json) =>
      TxCloudOcrParams(
        nonce: json["nonce"],
        userId: json["userId"],
        keyLicence: json["keyLicence"],
        appId: json["appId"],
        orderNo: json["orderNo"],
        sign: json["sign"],
        openApiAppVersion: json["openApiAppVersion"],
      );

  Map<String, dynamic> toJson() => {
    "nonce": nonce,
    "userId": userId,
    "keyLicence": keyLicence,
    "appId": appId,
    "orderNo": orderNo,
    "sign": sign,
    "openApiAppVersion": openApiAppVersion,
  };
}